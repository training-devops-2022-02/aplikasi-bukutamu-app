# Aplikasi Bukutamu #

1. Menjalankan database

    ```
    docker compose up
    ```

2. Menjalankan aplikasi

    ```
    mvn clean spring-boot:run
    ```

3. Build aplikasi menjadi jar

    ```
    mvn clean package -DskipTests
    ```

4. Build docker image

    ```
    docker build -t endymuhardin/aplikasi-bukutamu .
    ```

5. Run docker image

    ```
    docker run -e SPRING_DATASOURCE_URL=jdbc:postgresql://172.16.17.206/bukutamudb endymuhardin/aplikasi-bukutamu
    ```

6. Upload docker image ke registry

    ```
    docker tag endymuhardin/aplikasi-bukutamu endymuhardin/aplikasi-bukutamu:`git rev-parse --short HEAD`
    docker push -a endymuhardin/aplikasi-bukutamu
    ```