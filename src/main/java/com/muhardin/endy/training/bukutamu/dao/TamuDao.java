package com.muhardin.endy.training.bukutamu.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.muhardin.endy.training.bukutamu.entity.Tamu;

public interface TamuDao extends PagingAndSortingRepository<Tamu, Long> {
    
}
