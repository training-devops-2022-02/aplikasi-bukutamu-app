package com.muhardin.endy.training.bukutamu.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.endy.training.bukutamu.dao.TamuDao;
import com.muhardin.endy.training.bukutamu.entity.Tamu;

@RestController
public class TamuApiController {

    @Autowired private TamuDao tamuDao;

    @GetMapping("/api/tamu/")
    public Page<Tamu> semuaTamu(Pageable pageRequest){
        return tamuDao.findAll(pageRequest);
    }
}
