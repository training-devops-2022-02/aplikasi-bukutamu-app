create table tamu (
    id bigserial,
    email varchar(100) not null,
    nama varchar(200) not null,
    pesan varchar(255) not null,
    primary key (id), 
    unique (email)
);